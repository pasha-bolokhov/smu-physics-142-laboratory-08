%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                  H E A D E R                                 %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{header}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                D O C U M E N T                               %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\def\psep{4.0cm}
\def\qsep{3.0cm}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                                                              %
%                                   T I T L E                                  %
%                                                                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{center}
{  \huge \bf \color{red} Laboratory VIII  }\\[2mm]
{  \Large \textbf{\textit{\color{red} \cc{ \mathbold{R\,C} } Circuits}}  }\\[2mm]
{  \large \it \color{azure(colorwheel)} Introductory Physics II --- Physics 142/172L  }
\end{center}
\hrule
\bigskip
\begin{tabularx}{\textwidth}{lXr}
%
	&
	{\bf Name:}
	&
	{\bf Laboratory Section:}
	\hspace{1.0cm}
	\\[2mm]
%
	&
	{\bf Partners:}\hspace{5.0cm}
\end{tabularx}
\medskip


%\def\SHOWSOLUTIONS{1}
%\newcommand\solution[1]{{\ifdefined\SHOWSOLUTIONS\color{richelectricblue}#1\fi}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                            I N T R O D U C T I O N                           %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Introduction}}

	In this laboratory we will consider another electrical component
	with unique characteristics --- the capacitor.
	The laboratory will also provide practice constructing and interpreting graphs
	for the purpose of circuit analysis

\bigskip\noindent
	As you have learned, a capacitor in its simplest form is two parallel plates
	of conductive material, separated by a non--conductive material
	that prevents the plates from touching.
	Capacitance \cc{ C } is measured in \emph{farads} (\cc{ \si{\farad} })
	with typical values of capacitors being measured in \cc{ \si{\micro\farad} }.
	Capacitor labelling sometimes deviates from standard metric prefixes in that
	an upper case \cc{ \text{M} } is often used in place of the \cc{ \si{\micro} }
	symbol for ``micro'' (\cc{ \times 10^{-6} }).
	Do not confuse it with ``mega'' (\cc{ \times 10^{+6} }).
	Megafarad capacitors have not been manufactured yet, at least not in a commercial scale

\bigskip\noindent
	As an electrical potential (voltage) is placed across the capacitor,
	electric current flows from the voltage source, and
	as a result, charges build up on the plates of the capacitor.
	If connected to a DC source, the current will continue to flow until the charge built up
	across the capacitor reaches the maximum level of the capacitor for the source voltage.
	This charge will remain until a pathway is provided between the two plates for the charge to dissipate.
	A capacitor's value is determined by the amount of charge \cc{ Q } it will hold when charged
	to a particular voltage
\ccc
\[
	C	~~=~~	\frac Q V
	\ccb\,.
\]
\ccb

\vskip -0.4cm
\noindent
	There are two commonly used symbols for capacitors:
\begin{center}
\begin{circuitikz}[line width = 0.2mm, color = ProcessBlue]
	\draw 	(-1, 0) to [C] (1, 0);
\end{circuitikz}
	\qquad\qquad
	\raisebox{0.34cm}{and}
	\qquad\qquad
\begin{circuitikz}[line width = 0.2mm, color = ProcessBlue]
	\draw 	(1, 0) to [pC] (-1, 0);
\end{circuitikz}
	\raisebox{0.34cm}{.}
\end{center}

\noindent
	When connected to an AC source, the capacitor will likely never reach a full charge in one direction
	before the source polarity is reversed,
	and in this way, current flows more or less continuously through a capacitor in an AC circuit,
	depending on the value of the capacitor and the frequency of the input

\bigskip\noindent
	Additional information on this topic may be found in your textbook, and, guess what, on the internet


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                               P R O C E D U R E                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Procedure}}

	In this exercise, we will investigate the properties of a relatively simple circuit
	involving capacitors and resistors.
	The circuit is important because it has numerous applications
	and because it illustrates various techniques used in ``solving'' circuits

\bigskip\noindent
\begin{minipage}[c][4.0cm][t]{0.48\textwidth}
\begin{center}
\begin{circuitikz}[line width = 0.4mm, color = ProcessBlue]
\ctikzset{bipoles/thickness=1}
	\draw	
		(-1.5, 3)	to [C, l_ = \cc{ C }]		(-1.5, 0)
		(-1.5, 3)	to				(-0.25, 3)
				to [nos, o-o]			(0.25, 3)
				to				(1.5, 3)
				to [R, l = \cc{ R }]		(1.5, 0)
				to [short]			(-1.5, 0);
\end{circuitikz}
\end{center}
\end{minipage}
\begin{minipage}[c][4.0cm][t]{0.46\textwidth}
	\vskip 0.2cm
	A capacitor \cc{ C } has been charged to a voltage \cc{ V },
	so there is a charge \cc{ Q ~=~ C\,V } stored on the capacitor
\end{minipage}

\bigskip\noindent
\begin{minipage}[c][4.0cm][t]{0.48\textwidth}
\begin{center}
\begin{circuitikz}[line width = 0.4mm, color = ProcessBlue]
\ctikzset{bipoles/thickness=1}
	% circuit
	\draw	
		(-1.5, 3)	to [C, l_ = \cc{ C }]		(-1.5, 0)
		(-1.5, 3)	to				(-0.25, 3)
				to [short, o-o]			(0.25, 3)
				to 				(1.5, 3)
				to [R, l = \cc{ R }]		(1.5, 0)
				to [short]			(-1.5, 0);

	% bent arrow
	\draw [draw = ProcessBlue, ->, > = latex, rounded corners]
		(-1.75, 2.7)	--	(-1.75, 3.25)	--	(-1.2, 3.25);
\end{circuitikz}
\end{center}
\end{minipage}
\begin{minipage}[c][4.0cm][t]{0.50\textwidth}
	\vskip 0.1cm
	When the switch is closed, the charge flows around the circuit
	as current to equalize the two plates of the capacitor.
	We will describe the current as a function of time mathematically,
	and in practice, and show that the two descriptions are consistent
\end{minipage}

\bigskip\noindent
	Let us apply the Kirchhoff's First Law to the circuit,
\ccc
\[
	\sum\, V	~~=~~	0
	\ccb\,.
\]
\ccb
	As we go around the circuit in the clockwise direction
	(in the direction of the flow of current),
	the electric potential \emph{increases} on the capacitor.
	This is because the lower plate of the capacitor is negatively charged,
	while the upper plate is positively charged, which causes
	the current to flow from the upper plate to the lower plate
	through the switch and resistor.
	Thus, the voltage across the capacitor is a positive quantity,
\ccc
\[
	V_C	~~=~~	\frac q C
	\ccb\,,
\]
\ccb
	On the other hand, as we traverse the resistor,
	the electric potential \emph{decreases},
	because the current always flows in the direction
	of decrease of the potential.
	Therefore, the voltage across the resistor is a negative quantity,
\ccc
\[
	V_R	~~=~~	-\,I\, R
	\,.
\]
\ccb
	Plugging these quantities into the Kirchhoff's First Law,
\ccc
\[
	0	~~=~~	\sum\, V
		~~=~~	\frac q C  ~-~  I\, R
	\ccb\,,
\]
\ccb
	and using the fact that
\ccc
\[
	I	~~=~~	-\,\frac{dq}{dt}
\]
\ccb
	(meaning, current is the flow of charge over time),
	we find
\ccc
\[
	\frac q C  ~+~  \frac{dq}{dt}\,R	~~=~~	0
	\ccb\,,
	\qquad\qquad\qquad
	\Rightarrow
	\qquad\qquad\qquad
	\ccc
	\frac{dq}{q}	~~=~~	-\,\frac{dt}{R C}
	\ccb\,.
\]
\ccb
	The mathematical solution\footnote{See Appendix \ref{app}} to this equation is,
\ccc
\[
	I	~~=~~	I_0\, e^{-\,\frac{t}{RC}}
	\ccb\,,
	\qquad\qquad
	\text{where}~~
	\ccc
	I_0	~~=~~	\frac V R
	\,.
\]
\ccb

\begin{center}
\begin{tikzpicture}
	% axes
	\draw	[thick, draw = ProcessBlue, ->, > = {Stealth[length = 2.4mm]}]
		(0, 0)	--	(10, 0)	node [right, below, shift = {(1mm, -1mm)}] {\cc{ t }};
	\draw	[thick, draw = ProcessBlue, ->, > = {Stealth[length = 2.4mm]}]
		(0, 0)	--	(0, 4)	node [left, above] {\cc{ I }};

	% curves
	\draw	[very thick, draw = usccardinal, line cap = round]
		plot [id = discharge-1, samples = 200, domain = 0: 9]
		function { 3.6 * exp(- x / 2.4) };
	\draw	[very thick, draw = venetianred, line cap = round]
		plot [id = discharge-1, samples = 200, domain = 0: 9]
		function { 3.6 * exp(- x / 1.2) };
\end{tikzpicture}
\end{center}
\noindent
	The solution is a decaying exponential as shown in the graph above.
	The values of \cc{ R } and \cc{ C } determine how rapidly the current decays.
	The greater the value of \cc{ C }, the more charge there is to dissipate,
	and the greater the value of \cc{ R }, the less current will flow,
	both of which increase the time required

\bigskip\noindent
	\cc{RC} determines the time constant \cc{ \tau } of the circuit.
	It has units of time,
\ccc
\begin{align*}
	R	& ~~=~~	\frac V I
	\ccb\,,
	&
	\ccc
	C	& ~~=~~	\frac q V
	\ccb\,,
	&
	\ccc
	R\,C	& ~~=~~ \frac V I\, \frac q V
		~~=~~	\frac q I
	\ccb\,.
\end{align*}
\ccb
	We substitute the standard units for these quantities,
\ccc
\[
	[\, R\,C \,]	~~=~~	\frac{\si{\coulomb}}{\si{\ampere}}
			~~=~~	\frac{\si{\coulomb}}{\si{\coulomb\per\second}}
			~~=~~	\si{\second}
	\ccb\,.
\]
\ccb


\newcounter{partcount}
\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        P A R T   1   D I S C H A R G E                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\refstepcounter{partcount}
\label{discharge}
\subsection*{\textit{Part \arabic{partcount} --- Discharge}}

	The circuit for this portion of the laboratory is like as follows:

\begin{center}
\begin{circuitikz}[line width = 0.4mm, color = ProcessBlue]
\ctikzset{bipoles/thickness=1}
	\draw	
		(-4, +1.5)	to [american voltage source]	(-4, -1.5)
				--				(-2, -1.5)
				--				(-2, -1)
				to [C, -*]			(-2, 1);
	\draw	[line cap = round]
		(-2, 1)		--				(-2, 1.6);
	\draw	(-4, +1.5)	to [short, -o]			(-2.3, 1.5);
	\draw	(-1.7, 1.5)	to [R, o-]			(3, 1.5)
				to [ammeter]			(3, -1.5)
				--				(-2, -1.5);
\end{circuitikz}
\end{center}

\noindent
	Pick two combinations for the values of \cc{ R } and \cc{ C } and
	do the steps below.
	Choose the value of \cc{ R C } which will facilitate data collection ---
	a value of \cc{ \SI{0.1}\second } would make it difficult to take multiple readings
	within \cc{ 3 } -- \cc{ 4 } time constants

\begin{itemize}
\item[\color{alizarin}\ding{234}]
	Measure the voltage \cc{ V } of the power supply (or battery),
	the resistance \cc{ R } and the capacitance \cc{ C }
	(capacitance meters must be shared)

\item
	Move the switch first to the left to charge the capacitor (for a few seconds).
	Notice there is no resistor in the charging loop,
	so the capacitor will charge quickly

\item
	Then flip the switch to the right and start the time

\item[\color{alizarin}\ding{234}]
	Collect a set of data \cc{ (\,I,~ t\,) } for \cc{ 3 } or \cc{ 4 } time periods \cc{ \tau }
\end{itemize}

\noindent
	As always, we are looking to compare our observed values with the theoretical values,
	in this instance we will look at times.
	Starting with the equation
\ccc
\[
	I	~~=~~	I_0\, e^{-\,\frac{t}{RC}}
\]
\ccb
	we can see that using a graph to determine the time constant \cc{ \tau ~=~ R C }
	can be difficult since it is part of an exponent.
	If we re--arrange it to the general form
\ccc
\[
	y	~~=~~	m\,x  ~+~  b
	\ccb\,,
\]
\ccb
	where the slope \cc{ m } is some function of the time constant,
	we can then solve for the observed value:
\ccc
\[
	\frac{I}{I_0}	~~=~~	e^{-\,\frac{t}{RC}}
	\qquad\quad\ccb\Rightarrow\ccc\quad\qquad
	\ln \frac{I}{I_0}	~~=~~	-\,\frac{t}{RC}
	\qquad\quad\ccb\Rightarrow\ccc\quad\qquad
	\ln \frac{I}{I_0}	~~=~~	-\,\frac{1}{RC} \, t
	\ccb\,.
\]
\ccb
	Now if we graph \cc{ \ln \frac{I}{I_0} } on the vertical axis
	and \cc{ t } on the horizontal axis,
	the slope will equal \cc{ -\,\frac{1}{RC} }
\begin{itemize}
\item[\color{alizarin}\ding{234}]
	Create the graph, find the slope and solve for \cc{ \tau }

\item[\color{alizarin}\ding{234}]
	Compare that to the theoretical value
\end{itemize}

\bigskip\noindent
% Stop sign
\begin{minipage}[c][2.0cm][t]{0.08\textwidth}
\begin{center}
\hskip -0.4cm
\scalebox{4.0}{\color{alizarin}\Stopsign}
\end{center}
\end{minipage}
% Caution
\begin{minipage}[c][2.3cm][t]{0.90\textwidth}
	{\textit{\textbf{Important}:
	Complete Part~\ref{discharge} before moving on to Part~\ref{charge}.
	Change the multimeter lead connection from \cc{ \si{\ampere} } to the \cc{ \si{\volt}/\si{\ohm} } connection
	before proceeding.
	Failure to do so may damage the meter}}
\end{minipage}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        P A R T   2   C H A R G E   U P                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\refstepcounter{partcount}
\label{charge}
\subsection*{\textit{Part \arabic{partcount} --- Charge up}}

	The circuit for this part of the laboratory is shown below.
	Use the same components (resistor and capacitor) that you used for Part~\ref{discharge}

\begin{center}
\begin{circuitikz}[line width = 0.4mm, color = ProcessBlue]
\ctikzset{bipoles/thickness=1}
	\draw	
		(-4, +2)	to [american voltage source]	(-4, -2)
				to [R]				(2, -2)
				--				++(0, 1)
				to [C]				++(0, 2)
				to [short, -*]			++(0, 0.5);
	\draw	[line cap = round]
		(2, 1.5)	--				++(0, 0.6);
	\draw
		(-4, +2)	to [short, -o]			(1.7, 2)
		(2.3, 2)	to [short, o-]			(4, 2)
				|-				(2, -2);

	\draw	(2, 1)		--				++(-1.6, 0)
				to [voltmeter]			++(0, -2)
				--				++(1.6, 0);
\end{circuitikz}
\end{center}

\noindent
	For the same combinations of \cc{ R } and \cc{ C } you used in Part~\ref{discharge},
	do the following:
\begin{itemize}
\item
	First move the switch to the right to discharge the capacitor (for a few seconds).
	Note that there is no resistor in the discharge loop, so the capacitor discharges quickly

\item
	Then flip the switch to the left and start the time

\item[\color{alizarin}\ding{234}]
	Collect a set of data \cc{ (\,V_C,~ t\,) } for \cc{ 3 } or \cc{ 4 } time periods \cc{ \tau }
\end{itemize}

\noindent
	The mathematical solution for the current in this circuit is similar to the discharge equation
	from Part~\ref{discharge}.
	Using Kirchhoff's First Law,
\ccc
\[
	\sum\, V	~~=~~	0
			~~=~~	V_0  ~+~  V_C  ~+~  V_R
	\ccb\,.
\]
\ccb
	We traverse the circuit clockwise, starting from the battery.
	Since we cross the battery from its negative to its positive ends, the electric potential \emph{increases}.
	When we traverse the capacitor, its upper plate will be accumulating positive charge, and the lower plate
	--- negative charge.
	Thus, the potential \emph{decreases} on the capacitor,
\ccc
\[
	V_C	~~=~~	-\,\frac{q}{C}
	\ccb\,.
\]
\ccb
	Similarly, the potential \emph{decreases} on the resistor when we pass it in the clockwise direction,
	since it is connected to the negative terminal of the battery,
\ccc
\[
	V_R	~~=~~	-\,I\,R
	\ccb\,.
\]
\ccb
	We have,
\ccc
\[
	0	~~=~~	\sum\, V	~~=~~	V_0  ~~-~~  \frac q C  ~~-~~  I\, R
	\ccb\,.
\]
\ccb
	In other words,
\ccc
\[
	\frac q C  ~+~  I\, R	~~=~~	V_0
	\ccb\,.
\]
\ccb
	As the capacitor charges, the voltage across and the current through the resistor
	drops at the same rate determined by the time constant in Part~\ref{discharge}.
	The voltage across the capacitor then can be expressed as,
\ccc
\[
	|V_C|	~~=~~	\frac q C	~~=~~	V_0\, \big(\, 1 ~-~ e^{- t / \tau} \,\big)
	\ccb\,.
\]
\ccb
	So at \cc{ t ~=~ 1\,\tau }
	the value of voltage \cc{ |V_C| } will be \cc{ 0.632\,V_0 } \textit{etc}.
	Using a similar method to re--arrange the equation to allow us to solve for \cc{ RC }
	using a graph, we come up with
\ccc
\[
	\ln \lgr 1 \,-\, \frac {|V_C|}{V_0} \rgr	~~=~~	-\,\frac 1 {RC}\, t
	\ccb\,.
\]
\ccb
	Graphing \cc{ \ln \lgr 1 \,-\, \frac {|V_C|}{V_0} \rgr } on the vertical axis
	and \cc{ t } on the horizontal axis again allows us to solve for \cc{ RC } using the slope of the graph


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                A N A L Y S I S                               %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Analysis}}

	Graph your data from the discharging and charging circuits as described above.
\begin{itemize}
\item[\color{alizarin}\ding{234}]
	Compare the theoretical value of the time constant \cc{ RC } to the observed values
	for both the charging and discharging circuits found using the graphs

\item[\color{alizarin}\ding{234}]
	Compare the observed values of the time constant \cc{ RC } for the charging
	circuit and the discharging circuit with the same combinations of \cc{ R } and \cc{ C }.
	If both circuits used the same combinations of \cc{ R } and \cc{ C }, should they match?
\end{itemize}


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                A P P E N D I X                               %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\renewcommand{\thesection}{\Alph{section}}
\section{\textit{Solving the differential equation}}
\label{app}

	We will solve the differential equation for the discharging capacitor only,
	but the solution process is identical for the charging capacitor.
	For the discharging capacitor, we found from the Kirchhoff's First Law
\ccc
\[
	\frac{dq}{q}	~~=~~	-\,\frac{dt}{R C}
	\,.
\]
\ccb
	This equation is solved by the method of \emph{separation of variables}.
	In the form written above, the equation is already separated ---
	the left--hand side only involves the unknown function \cc{ q },
	while the right-hand side only involves time \cc{ dt }.
	We integrate both sides,
\ccc
\[
	\ln q	~~=~~	-\,\frac t {RC}  ~+~  \text{const}
	\ccb\,.
\]
\ccb
	Now we exponentiate both sides to get rid of the logarithm,
\ccc
\[
	e^{\ln q}	~~=~~	e^{-\frac t {RC} ~+~ \text{const}}
			~~=~~	e^\text{const}\, e^{-\frac{t}{RC}}
	\ccb\,.
\]
\ccb
	Since \cc{ e^{\ln x} ~=~ x },
\ccc
\[
	q	~~=~~	e^\text{const}\, e^{-t / \tau}	~~=~~	\text{const} \cdot e^{-\,t / \tau}
	\ccb\,,
	\qquad\qquad
	\text{where}
	\ccc
	\qquad
	\tau	~~=~~	RC
	\ccb\,.
\]
\ccb
	Here we have used the fact that \cc{ e^\text{const} } is just a different constant.
	That constant is really equal to \cc{ q_0 }, since at \cc{ t ~=~ 0 },
\ccc
\[
	q_0	~~\equiv~~	q(0)	~~=~~	\text{const} \cdot e^{0}	~~=~~	\text{const} \cdot 1
	\ccb\,.
\]
\ccb
	So we find,
\ccc
\[
	q(t)	~~=~~	q_0\,e^{-\, t/\tau }
	\ccb\,,
\]
\ccb
	and since \cc{ q_0 ~=~ C V_0 },
\ccc
\[
	q(t)	~~=~~	C V_0\,e^{-\, t/\tau }
	\ccb\,.
\]
\ccb
	Differentiating both sides with respect to time, we find,
\ccc
\[
	I	~~=~~	\Big|\, \frac{dq}{dt} \,\Big|	~~=~~	C V_0\,\frac 1 \tau\, e^{ -\, t / \tau }
		~~=~~	\frac{ C V_0 }{ R C }\,e^{ -\, t / \tau }
	\ccb\,,
\]
\ccb
	and hence,
\ccc
\[
	I	~~=~~	I_0\,e^{-\, t / \tau}
	\ccb\,.
\]
\ccb


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                   L A B O R A T O R Y   E V A L U A T I O N                  %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Laboratory evaluation}}

	Please provide feedback on the following areas, comparing this laboratory to your previous labs.
	Please assign each of the listed categories a value in 1 ~--~ 5, with 5 being the best, 1 the worst.

\bigskip
\begin{itemize}
\item
	how much fun you had completing this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	how well the lab preparation period explained this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	the amount of work required compared to the time allotted?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	your understanding of this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	the difficulty of this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	how well this laboratory tied in with the lecture?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5
\end{itemize}

\bigskip\noindent
	Comments supporting or elaborating on your assessment can also be very helpful in improving the future laboratories


\end{document}
